<?php

/**
 *
 * СЕРВЕР - запускать из под рута  - так как создается сервер с портом WebSocket
 *  - ИСПОЛЬЗУЕТСЯ: Ratchet(WebSocket server), PAMI(php AMI (Asterisk) Interface
 *
 * */




/**
 * Включить на случай проверки - в консоли не все проверки показывать будет
 */
ini_set('display_errors',1);
error_reporting(E_ALL);


/**
 * Подключем классы
 */
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/selfclass/AsteriskLogger.php';
require __DIR__ . '/selfclass/AsteriskIoServer.php';
require __DIR__ . '/selfclass/ResponseReceivedWS.php';
require __DIR__ . '/selfclass/WebsocketClients.php';




/**
 * Юзинги
 */
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use selfclass\AsteriskIoServer;
use selfclass\WebsocketClients;


/**
 * PAMI настройки
 */
$pamiClientOptions = array(
    'host' => '127.0.0.1',
    'scheme' => 'tcp://',
    'port' => 5038,
    'username' => 'login',
    'secret' => 'password',
    'connect_timeout' => 10000,
    'read_timeout' => 10000
);




/**
 * Объект клиентов подключенных по websocket
 */
$clients = new WebsocketClients();

/**
 * Ratchet SERVER
 */
$server = null;

/**
 * AsteriskIoServer SERVER OVVERIDE
 */
$asterisk = null;


/**
 * Функция инициализации и запуска сервера
 * - создана для того что бы  перезапускать можно было
 */
function InitializeServer(){
    global $server, $clients, $pamiClientOptions, $asterisk;

    try {

        /**
         * инициализируем App(Retchet)->server и вешаем на порт
         */
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $clients
                )
            ), 8085
        );



        /**
         * накидываем наш объект на созданный Ratchet Server для обработки всех нужных данных
         *      - Синхронизируем таймер с таймером Asterisk для получения данных
         *      - Если нужно изменения делать - то в нем
         */
        $asterisk = new AsteriskIoServer($server, $clients, $pamiClientOptions);

        /**
         * Сервер(Ratchet) - НА СТАРТ - ВНИМАНИЕ - МАРШ
         */
        $server->run();
    } catch (Exception $e){
        /**
         * Что-то не так - Не вырубаем срипт - ожидаем секунду
         *          - кстати часто ругается на socket->read когда пустой
         */
        var_dump($e->getMessage());

        /**
         * Пишем в лог
         */
        file_put_contents("logs/server_error.log", $e, FILE_APPEND);


        /**
         * Если доэтого были присоедененные клиенты webSocket то дисконектим их
         */
        $clients->closeAll();

        if($asterisk){
            unset($asterisk);
            $asterisk = null;
        }


        /**
         * При перезапуске - если уже было соединение то старое грохнуть надо
         */
        if($server){
            if($server->socket)
                $server->socket->shutdown();
            unset($server);
            $server = null;
//
//            usleep(1000);
        }


        /**
         * Спим секунду
         */
        usleep(1000);

        /**
         * Перезапускаем сервер
         */
        InitializeServer();
    }
}


/**
 * Сервер-карате на запуск!!!
 */
InitializeServer();



?>