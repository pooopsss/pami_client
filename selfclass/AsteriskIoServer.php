<?php


/**
 * namespace
 */
namespace selfclass;


/**
 * Юзинги
 */
use PAMI\Client\Impl\ClientImpl as PamiClient;
use \PAMI\Message\Event\DialEvent;
use \PAMI\Message\Event\BridgeEvent;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\HangupEvent;
use PAMI\Message\Response\ResponseMessage;
use Ratchet\Wamp\Exception;
use Ratchet\Server\IoServer;
use React\EventLoop\LoopInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use PAMI\Message\Action\SIPShowPeerAction;
use PAMI\Message\Action\SIPPeersAction;
use PAMI\Message\Action\ActionMessage;
use PAMI\Message\Event\PeerEntryEvent;
use PAMI\Message\Event\PeerStatusEvent;
use PAMI\Message\Event\StatusCompleteEvent;
use PAMI\Message\Event\AgentConnectEvent;
use PAMI\Message\Event\RegistryEvent;
use PAMI\Message\Event\QueueMemberStatusEvent;






class SIPShowPeersAction extends ActionMessage
{

    public function __construct()
    {
        parent::__construct('SIPshowpeers');
    }
}


/**
 * Класс - AsteriskIoServer
 * для синхронизации и обработки от IoServer
 */
class AsteriskIoServer {

    /**
     * Клиент PAMI
     */
    private $pamiClient;

    /**
     * Интервал работы Retchet server - секунды с лихвой хватает - можно 0.1 , - float,int
     */
    private $interval = 0.1;

    /**
     * PAMI настройки
     */
    private $pamiClientOptions = [];

    /**
     * PAMI - открыт/закрыт
     */
    private $is_opened = false;

    /**
     * Ratched - IOServer
     */
    private $server = null;

    /**
     * WebsocketClients class
     */
    private $clients = null;




    /**
     * Constructor
     */
    public function __construct(IoServer &$server, WebsocketClients &$clients, $pamiClientOptions) {

        /**
         * Устанавливаем значения
         */
        $this->pamiClientOptions = $pamiClientOptions;
        $this->server = $server;
        $this->pamiClient = new PamiClient($this->pamiClientOptions);
        $this->clients = $clients;


        /**
         * Можно установить лог $this->pamiClient->setLogger(new AsteriskLogger());
         */
        $this->pamiClient->setLogger(new AsteriskLogger());



        try {

            /**
             * Устанавливаем соединение с PAMI
             */
            $this->pamiClient->open();
            $this->is_opened = true;


            /**
             * Устанавливаем данные $currentStates на текущий момент запуска
             */
            $this->_setCurrentStateNumbers();

            /**
             * Регистрируем для обработки нужные события
             */
            $this->_registerEvents();

            /**
             * Соединяем таймеры Ratched и PAMI
             */
            $this->_prepareLoop();

        } catch(Exception $e){

            /**
             * Если что то не то - то флаг открытия сбрасываем
             */
            $this->is_opened = false;
        }

    }


    /**
     * Получение ссылки на сервер
     */
    public function &getServer(){
        return $this;
    }


    /**
     * Получение ссылки на PAMI Client
     */
    public function &getPAMIClient(){
        return $this->pamiClient;
    }


    /**
     * Получение номеров со статусом OK
     *  - После PAMI->open()
     */
    protected function _setCurrentStateNumbers(){
        if( $this->clients->currentStates )
            return;
        /**
         * Очищаем массив текущих стейтов
         */
        $this->clients->currentStates = [];

        /**
         * Если объект как объект
         */
        if($this->pamiClient) {

            /**
             * Создаем екшен SIPPeersAction
             */
            $act = new SIPPeersAction();

            /**
             * Отправляем екшен в PAMI
             */
            $response = $this->pamiClient->send($act);

            /**
             * Если ответ валидный
             */
            if($response->isSuccess()){

                /**
                 * Получаем массив событий
                 */
                $eventsList = $response->getEvents();

                /**
                 * Если массив и есть хотя бы одна запись
                 */
                if($eventsList){

                    /**
                     * Перебираем массив событий
                     */
                    foreach($eventsList as $itemEv){

                        /**
                         * Если событие относится PeerEntryEvent
                         */
                        if($itemEv instanceof PeerEntryEvent){

                            /**
                             * Если статус OK (substr для того что этот дебил возвращает : "OK (22 sec)" ) в случе OK
                             */
                            if(substr($itemEv->getStatus(),0,2) == "OK"){

                                /**
                                 * Если статус OK то устанавливаем в таблицу стейтов что активен
                                 *  - поумолчанию все выключены
                                 */
                                $this->_setCurrentStateNumber($itemEv->getObjectName(), "");
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * Проверка на - будет ли изменен статс или номер что бы отправить мессагу как правило
     */
    protected function _checkChangeStateNumber($src, $eventName, $registred=false){

        /**
         * Поиск - есть ли такой номер уже в массиве
         */
        foreach( $this->clients->currentStates as $k=>$v ){

            /**
             * Нашли - получаем индекс массива  на SRC запись
             */
            if($v['num'] == $src){
                if($v['event'] == $eventName)
                    return false;
                if(empty($v['event']) && $eventName == "Registered")
                    return false;
            }
        }

        /**
         * По умолчанию номер новый добавлен или добнавлен статус
         */
        return true;
    }

    /**
     * Получение ссылки на сервер
     */
    protected function _setCurrentStateNumber($src, $eventName){

        /**
         * Индекс найденого номера если есть
         */
        $keySRCFounded = -1;

        /**
         * Поиск - есть ли такой номер уже в массиве
         */
        foreach( $this->clients->currentStates as $k=>$v ){

            /**
             * Нашли - получаем индекс массива  на SRC запись
             */
            if($v['num'] == $src){
                $keySRCFounded = $k;
            }
        }

        /**
         * Если длина записи равна 3(100....999)
         */
        if(mb_strlen($src) == 3){

            /**
             * Если индекс валидный то устанваливаем туда , если нет создавем новый номер
             */
            if($keySRCFounded >=0){
                $this->clients->currentStates[$keySRCFounded] = ['num'=>$src, 'event'=>$eventName];
            } else {
                $this->clients->currentStates[] = ['num'=>$src, 'event'=>$eventName];
            }
        }

    }


    /**
     * для Bridge. так как номер может быть с очереди, то в таком случае в channels есть данные
     */
    protected function _chanellToPeer($source, $channel){
        /*
         *  Channel1: SIP/106-0000029c
            Channel2: SIP/FXO-0000029d
            Channel2: Local/101@from-queue-0000027d;1
         * */

        if(mb_substr($channel,7,1) == "-") {
            if (mb_substr($channel, 0, 4) == "SIP/") {
                $grayLine = mb_substr($channel, 0, 7);
                $epxl = explode("/", $grayLine);
                if (count($epxl) == 2) {
                    if (is_numeric($epxl[1])) {
                        return $epxl[1];
                    }
                }
            }
        }
        if(mb_substr($channel,9,1) == "@") {
            if (mb_substr($channel, 0, 6) == "Local/") {
                $grayLine = mb_substr($channel, 0, 9);
                $epxl = explode("/", $grayLine);
                if (count($epxl) == 2) {
                    if (is_numeric($epxl[1])) {
                        return $epxl[1];
                    }
                }
            }
        }


        return $source;
    }


    /**
     * Подключаемся к событиям PAMI, слушаем их и обрабатываем как надо
     */
    protected function _registerEvents(){

        /**
         * Если соединение PAMI открыто - ЧИКИПУКИ
         */
        if($this->is_opened){

            /**
             * Обязателно это делаем, что бы передать в registerEventListener иначе исключение в некоторых случаях кидает
             *  - хотя и $this->clients работает
             */
            $clients = $this->clients;


            /**
             * Подключаемся ко ВСЕМ событиям PAMI
             *  - Разные события могут иметь разные методы и в консоли ошибку невидать (((
             *  - Если нужно что бы в клиент отправлялся еще какое событие то добавить обработчик в ResponseReceivedWS
             */
            $this->pamiClient->registerEventListener(function (EventMessage $event)  use(&$clients)  {


                /**
                 * DIAL
                 *  - срабатывает когда еще нет соединения A->B но начал набирать
                 *  - subEvent = Begin|End срабатывает при начале набора и конце перед Hangup
                 *
                 * ----- Мы обрабатываем Begin так как конец ловим Hangup'ом так как там больше инфы
                 */
                if($event instanceof DialEvent){
                    if($event->getSubEvent() == "Begin"){
                        $clients->setMessageAllUsers([
                            'src'=>$event->getCallerIDNum(),
                            'dest'=>$event->getKey('connectedlinenum'),
                            'name'=>$event->getName()
                        ]);

                        /**
                         * Устанавливаем текущее состояние короткому номеру
                         */
                        $this->_setCurrentStateNumber($event->getCallerIDNum(), $event->getName());
                        $this->_setCurrentStateNumber($event->getKey('connectedlinenum'), $event->getName());
                    }

                }





                /**
                 * HANGUP
                 *  - срабатывает когда связь разрывается
                 *
                 * ----- Тут и даем понять вместо Dial (End) что тут завершается разговор - тут просто инфы в event больше
                 */
                if($event instanceof HangupEvent){
                    $src = $event->getCallerIDNum();
                    $dst = $event->getKey('connectedlinenum');
                    $ch = $event->getChannel();

                    if(mb_strlen($src) > 3){
                        $src = $this->_chanellToPeer($src,$ch);
                    }
                    if(mb_strlen($dst) > 3){
                        $dst = $this->_chanellToPeer($dst,$ch);
                    }
                    $clients->setMessageAllUsers([
                        'src'=>$src,
                        'dest'=>$dst,
                        'name'=>$event->getName()
                    ]);


                    /**
                     * Устанавливаем текущее состояние короткому номеру
                     */
                    $this->_setCurrentStateNumber($src, $event->getName());
                    $this->_setCurrentStateNumber($dst, $event->getName());
                }





                /**
                 * BRIDGE
                 *  - срабатывает когда связь установилась между A и B и пошли секунды разговора
                 *
                 * ----- Отправляем событие что именно идет разговор а не набор номера
                 */
                if($event instanceof BridgeEvent){

                    /**
                     * Устанавливаем переменные
                     */
                    $src = $event->getCallerID1();
                    $dst = $event->getCallerID2();
                    $ch1 = $event->getChannel1();
                    $ch2 = $event->getChannel2();
                    /*
                     * Channel1: SIP/106-0000029c
                     * Channel2: SIP/FXO-0000029d
                     * */

                    /**
                     * Если номера мобильные пытаемся вытащить с Channels
                     */
                    if(mb_strlen($src) > 3){
                        $src = $this->_chanellToPeer($src,$ch1);
                        if(mb_strlen($src) > 3){
                            $src = $this->_chanellToPeer($src,$ch2);
                        }
                    }
                    if(mb_strlen($dst) > 3 && mb_strlen($src) > 3){
                        $dst = $this->_chanellToPeer($dst,$ch1);
                        if(mb_strlen($src) > 3){
                            $src = $this->_chanellToPeer($dst,$ch2);
                        }
                    }


                   // var_dump($ch1.":::".$ch2."  ".$src."   ".$dst."  ".$event->getBridgeState());
                    if($this->_checkChangeStateNumber($src, $event->getName())) {
                        $clients->setMessageAllUsers([
                            'src' => $src,
                            'dest' => $dst,
                            'name' => $event->getName(),
                            'state' => $event->getBridgeState()
                        ]);


                        /**
                         * Устанавливаем текущее состояние короткому номеру Если соединение уже установлено и еще идет
                         */
                        //if($event->getBridgeState() == "Link"){
                        $this->_setCurrentStateNumber($src, $event->getName());
                        $this->_setCurrentStateNumber($dst, $event->getName());
                        //}
                    }


                }






                /**
                 * PEAR STATUS
                 *  - срабатывает по таймеру в настройках aster просто бегает и пингует и отправляет статус пира
                 * --- Очень осторожно так как этот говнюк может перезаписать другие статусы
                 */
                if($event instanceof PeerStatusEvent){

                    /**
                     * explode name
                     *  - SIP/101
                     */
                    $peerExpl = explode("/",$event->getPeer());

                    /**
                     * PeerStatusEvent.status (Registered | Unreachable)
                     */
                    $statusText = $event->getPeerStatus();

                    /**
                     * Если разбито правильно $peerExpl[0] = SIP, $peerExpl[1] = 101
                     */
                    if(count($peerExpl) == 2){


                        /**
                         * Отправляем сообщение о статусе юзера
                         */
                        //var_dump($peerExpl[1]."  ".$statusText."   ".$this->_checkChangeStateNumber($peerExpl[1], $statusText, true));
                        if($this->_checkChangeStateNumber($peerExpl[1], $statusText, true)){
                            $clients->setMessageAllUsers([
                                'src'=>$peerExpl[1],
                                'dest'=>"",
                                'status'=>$statusText,
                                'name'=>$event->getName()
                            ], ResponseReceivedWS::MESSAGE_TYPE_PEER );

                            /**
                             * Устанавливаем текущее состояние короткому номеру
                             */
                            $this->_setCurrentStateNumber($peerExpl[1], $statusText);
                        }




                    }
                }





                /**
                 * -----------------------------------------------------------------------------------------------------------
                 * Следующие классы не найдены в библиотеки, хотя сообщения обрабатываются
                 *  - как появятся Confbridge* поставить код как выше
                 * -----------------------------------------------------------------------------------------------------------
                 */



                /**
                 * ConfbridgeJoin
                 *  - срабатывает когда создается или подключается пир к конференции
                 *
                 * ----- Срабатывает всегда, даже когда пир создает конференцию
                 */
                if($event->getName() == "ConfbridgeJoin"){
                    $clients->setMessageAllUsers([
                        'src'=>$event->getKey('calleridnum'),
                        'dest'=>"",
                        'name'=>$event->getName(),
                    ]);


                    /**
                     * Устанавливаем текущее состояние короткому номеру
                     */
                    $this->_setCurrentStateNumber($event->getKey('calleridnum'), $event->getName());

                }


                /**
                 * ConfbridgeLeave
                 *  - срабатывает при отключении пира от конференции
                 */
                if($event->getName() == "ConfbridgeLeave"){
                    $clients->setMessageAllUsers([
                        'src'=>$event->getKey('calleridnum'),
                        'dest'=>"",
                        'name'=>$event->getName(),
                    ]);


                    /**
                     * Устанавливаем текущее состояние короткому номеру
                     */
                    $this->_setCurrentStateNumber($event->getKey('calleridnum'), $event->getName());

                }


               // var_dump($event->getName());


            });
        }
    }


    /**
     * Подключаемся к tick Retched сервера и обрабатываем PAMI->Process();
     */
    protected function _prepareLoop(){

        /**
         * Обязателно это делаем, что бы передать в addPeriodicTimer иначе исключение в некоторых случаях кидает
         *  - хотя и $this->pamiClient работает
         */
        $pamiClient = $this->pamiClient;


        /**
         * Переопределяем loop обработчик Retched server своим
         */
        $this->server->loop->addPeriodicTimer($this->interval, function () use (&$pamiClient)  {

            try {

                /**
                 * Обрабатываем запросы PAMI
                 */
                $this->pamiClient->process();
                //var_dump("______________________PROCESS__________________________\n");
                //usleep(1000);

            } catch (Exception $exc) {

                var_dump($exc);
                $this->clients->closeAll();
                /**
                 * Если ошибка заново пробуем открыть
                 */
                $this->pamiClient->open();
            }
        });
    }



    /**
     * Destructor
     */
    public function __destruct() {

        /**
         * Если Retched Server(IoServer) еще запущен, то останавливаем
         */
        if($this->server){
            $this->server->loop->stop();
        }


        /**
         * Если PAMI соеденен, то отсоединяем
         */
        if ($this->pamiClient && $this->is_opened) {
            $this->pamiClient->close();
        }
    }

}
