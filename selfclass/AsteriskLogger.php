<?php
/**
 * namespace
 */
namespace selfclass;



/**
 * Юзинги
 */
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;



/**
 * Класс - AsteriskLogger
 * Логирование PAMI -> сервера
 */
class AsteriskLogger implements  LoggerInterface
{

    public function emergency($message, array $context = array())
    {
        // TODO: Implement emergency() method.
    }

    public function alert($message, array $context = array())
    {
        // TODO: Implement alert() method.
    }

    public function critical($message, array $context = array())
    {
        // TODO: Implement critical() method.
        var_dump($message);
    }

    public function error($message, array $context = array())
    {
        // TODO: Implement error() method.
        var_dump($message);
    }

    public function warning($message, array $context = array())
    {
        // TODO: Implement warning() method.
    }

    public function notice($message, array $context = array())
    {
        // TODO: Implement notice() method.
    }

    public function info($message, array $context = array())
    {
        // TODO: Implement info() method.
    }

    public function debug($message, array $context = array())
    {
        // TODO: Implement debug() method.
        //var_dump($message);
    }

    public function log($level, $message, array $context = array())
    {
        // TODO: Implement log() method.
    }
}