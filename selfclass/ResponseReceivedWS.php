<?php

/**
 * namespace
 */
namespace selfclass;


/**
 * Юзинги
 */
use \PAMI\Message\Event\DialEvent;
use \PAMI\Message\Event\BridgeEvent;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\HangupEvent;


/**
 * Класс - ResponseReceivedWS
 * для формирования сообщения клиентам
 */
class ResponseReceivedWS {

    /**
     * CONST: тип сообщения
     */
    const MESSAGE_TYPE_DEFAULT = "";
    const MESSAGE_TYPE_EVENT = "event";
    const MESSAGE_TYPE_LIST = "list";
    const MESSAGE_TYPE_PEER = "peer";


    /**
     * Тело сообщения
     */
    private $body = null;


    /**
     * Тип сообщения("","event"....и т.д)
     */
    private $typeMessage = ResponseReceivedWS::MESSAGE_TYPE_DEFAULT;


    /**
     * Конструктор
     * @param $message
     * @param string $type
     */
    public function __construct($message, $type =  ResponseReceivedWS::MESSAGE_TYPE_DEFAULT) {

        /**
         * Устанавливаем тип
         */
        $this->typeMessage = $type;


        /**
         * Если сообщение
         */
        if(is_object($message)){
            if($message instanceof EventMessage){
                /**
                 * устанваливаем Body
                 * -DEPRECATED
                 */
                $this->EventMessageToResponse($message);
            } else {

                /**
                 * устанваливаем Body
                 */
                $this->body = $message;//json_encode($message);
            }
        }


        /**
         * если Массив
         */
        if(is_array($message)){
            $this->body = $message;
        }


        /**
         * если страка просто устанавливаем в body эту строку
         */
        if(is_string($message)){
            $this->body = $message;
        }
    }





    /**
     * Функция установки в body сообщения с нужными полями по типу сообщения
     * - DEPRECATED - обработку перенс в AsteriskIoServer
     */
    protected function EventMessageToResponse(EventMessage $event){

        /**
         * Устанваливаем тип сообщения - СОБЫТИЕ
         */
        $this->typeMessage = ResponseReceivedWS::MESSAGE_TYPE_EVENT;

        /**
         * Устанваливаем BODY в NULL что бы предыдущие записи никакие не мешали
         */
        $this->body = null;


        /*------------------------------- EVENT REDECLARE START ----------------------------------------------------------------------*/

        /**
         * DIAL
         *  - срабатывает когда еще нет соединения A->B но начал набирать
         *  - subEvent = Begin|End срабатывает при начале набора и конце перед Hangup
         *
         * ----- Мы обрабатываем Begin так как конец ловим Hangup'ом так как там больше инфы
         */
        if($event instanceof DialEvent){
            $this->body = [
                'src'=>$event->getCallerIDNum(),
                'dest'=>$event->getKey('connectedlinenum'),
                'name'=>$event->getName()
            ];
        }



        /**
         * HANGUP
         *  - срабатывает когда связь разрывается
         *
         * ----- Тут и даем понять вместо Dial (End) что тут завершается разговор - тут просто инфы в event больше
         */
        if($event instanceof HangupEvent){
            $this->body = [
                'src'=>$event->getCallerIDNum(),
                'dest'=>$event->getKey('connectedlinenum'),
                'name'=>$event->getName()
            ];
        }



        /**
         * BRIDGE
         *  - срабатывает когда связь установилась между A и B и пошли секунды разговора
         *
         * ----- Отправляем событие что именно идет разговор а не набор номера
         */
        if($event instanceof BridgeEvent){
            $this->body = [
                'src'=>$event->getCallerID1(),
                'dest'=>$event->getCallerID2(),
                'name'=>$event->getName(),
                'state'=>$event->getBridgeState()
            ];
        }



        /*------------------------------- EVENT REDECLARE END ----------------------------------------------------------------------*/




        /**
         * Если событие не обработано
         */
        if($this->body == null){
            /**
             * Устанваливаем typeMessage в default
             */
            $this->typeMessage = ResponseReceivedWS::MESSAGE_TYPE_DEFAULT;

            /**
             * в Body пишем название события которое пришло но установлено в это классе
             */
            $this->body = $event->getName()." Not supported in ResponseRecievedWS";
        }
    }






    /**
     * Функция формирования сообщения
     */
    public function getJsonMessage(){
        return json_encode([
            'status'=>'ok',
            'body'=>$this->body,
            'type'=> $this->typeMessage,
        ]);
    }






    /**
     * Destructor
     */
    public function __destruct() {

        /**
         * Очищаем BODY
         */
        unset($this->body);
    }

}
