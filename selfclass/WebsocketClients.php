<?php

/**
 * namespace
 */
namespace selfclass;


/**
 * Юзинги
 */
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use PAMI\Message\Action\SIPShowPeerAction;
use PAMI\Message\Action\SIPPeersAction;
use PAMI\Message\Action\QueueStatusAction;
use PAMI\Message\Action\QueueSummaryAction;

use PAMI\Message\Action\CommandAction;
use PAMI\Message\Action\ActionMessage;
use PAMI\Message\Response\ResponseMessage;

use PAMI\Message\Event\PeerEntryEvent;

/**
 * Класс - WebsocketClients
 * для отправки/получения клиентам
 */
class WebsocketClients implements MessageComponentInterface {

    /**
     * SplObjectStorage - Список подключенных колиентов
     */
    protected $clients;


    /**
     * Array - состояние номеров текущее
     */
    public $currentStates = [];




    /**
     * Конструктор
     */
    public function __construct() {

        /**
         * Устанавливаем Список подключенных колиентов
         */
        $this->clients = new \SplObjectStorage;
        //echo "Congratulations! the server is now running\n";
    }





    /**
     * onOpen срабатывает когда websocket клиент подключился к серверу
     */
    public function onOpen(ConnectionInterface $conn) {

        /**
         * Добавляем пользователя в общий список клиентов
         */
        $this->clients->attach($conn);


        /**
         * Если currentStates не пустое место, то получаем список всех статусов пиров
         */
         if($this->currentStates){

             /**
              * готовим мессагу к нужному формату
              */
             $msgReady = (new ResponseReceivedWS($this->currentStates, ResponseReceivedWS::MESSAGE_TYPE_LIST))->getJsonMessage();

             /**
              * отправляем лист стейтов толькочто подключенному УСЁРУ
              */
             $conn->send($msgReady);
         }


    }


    /**
     * onMessage Функция срабатывает, когда WebSocket клиент отправил сообщение серверу
     */
    public function onMessage(ConnectionInterface $from, $msg) {
        //$numRecv = count($this->clients) - 1;
        /*echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');*/

        //TODO: тут обработка сообщения нужна и соответсвтующие действия


        /**
         * Получаем вид сообщения от ResponseReceivedWS
         */
        $msgReady = (new ResponseReceivedWS($msg))->getJsonMessage();

        /**
         * Отправляем всем клиентам сообщение, кроме исходного
         */
        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $client->send($msgReady);
            }
        }
    }



    /**
     * Отправляем сообщение всем подключенным клиентам
     */
    public function setMessageAllUsers($msg, $type = ResponseReceivedWS::MESSAGE_TYPE_EVENT){

        /**
         * Получаем вид сообщения от ResponseReceivedWS
         */
        $msgReady = (new ResponseReceivedWS($msg, $type))->getJsonMessage();


        /**
         * Отправляем всем сообщение
         */
        foreach ($this->clients as $client) {
            $client->send($msgReady);
        }
    }



    /**
     * onClose Когда клиент дисконектился
     */
    public function onClose(ConnectionInterface $conn) {

        /**
         * Убираем клиента с общего списка
         */
        $this->clients->detach($conn);
        //echo "Connection {$conn->resourceId} has disconnected\n";
    }


    public function closeAll(){
//var_dump($this->clients->count());
        foreach ($this->clients as $client) {
            $client->close();
            $this->clients->detach($client);
        }
        $this->clients->removeAll($this->clients);
    }


    /**
     * onError Когда при отправке пользователю произошла ошибка
     */
    public function onError(ConnectionInterface $conn, \Exception $e) {
        //echo "An error has occurred: {$e->getMessage()}\n";
        /**
         * Закрываем соединение с пользователем
         */
        $conn->close();
    }

}